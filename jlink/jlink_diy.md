# 一 综述
JLINK在我们调试低端CPU时候常见的调试器。串口也是在我们调试过程中经常使用的端口。那么在继承一个HUB坐一起岂不是个利器。废话不多说上图。
![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/jlink_diy/res/全图-正面.png "")  
<center>图 1.1 全图正面 </center>

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/jlink_diy/res/全图-背面.png "")  
<center>图 1.2 全图背面 </center>

# 二 资料
 请关注并回复
![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/jlink_diy/res/微信二维码.jpg "")  
<center>图 2.1 微信二维码  </center>

# 三 介绍


![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/jlink_diy/res/原理图.png "")  
<center>图 2.1 原理图  </center>
这里不做太多介绍，仅仅解释几点。

1.这个JLINK我只做了SW接口，因为线少，方便插。
2.串口和SW没有坐一起，害怕单独使用。
3.接口都加了保险丝，害怕烧了一连串的烧。
4.LMS1117有点发热，尽量不要用这个板子供电。】

如何使用？
1.先有个JLINK烧写器。
2.将下载资料中的hex文件下载到板子中，见第一个图有几个插针，就是专门下载板子的hex。引脚在丝印层都有说明。
3.下载完可能版本过老，会在MDK中提示，忽略即可。