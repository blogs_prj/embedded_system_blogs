# 一 综述
&emsp;BCB这个名字是我本来从平衡车控制主板（BALANCE CAR BOARD）演变过来，后来控制了很多车，比如双轮差速，单轮驱动，双轮平衡，四轮麦克纳姆轮等等。因此这个板子可以用在很多场合。当然你可以拷贝很多硬件原理图和代码。岂不是爽歪歪。如下图是整个主板示意图。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/主图.png "")  
<center>图 1.1 主图 </center>  

# 二 资料
&emsp;如果需要资源，请关注公众号并回复“BCB开源”，即可得到硬件和源码。源码我使用MDK编辑，硬件使用AD18。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/微信二维码.jpg "")  
<center>图 2.1 微信二维码 </center>  

# 三 介绍

## 3.1 硬件

### 3.1.1 电源

&emsp;
1.电源是12-35V输入，极限应该是48V，但是这个芯片可以承载5A电流。因此整个主板是绰绰有余；  
2.电源接口部分有两个保险丝和一个二极管，主要是用于保险作用。这里没有使用MOS作为保护；  
3.TPS54560B 芯片是TI家族内置MOS的降压开关电源芯片。不发热，唯一的缺点是配套的二极管太大了；  
4.整个模拟和数字是做了隔离，在5V输出又增加了一个0.5A的保险丝。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/电源.png "")  
<center>图 3.1 电源 </center>  

### 3.1.2 仿真器&调试器

&emsp;本主板集成了仿真器和调试器，如图，介绍如下。  
1.主要增加了HUB USART JLINK；  
2.HUB使用的芯片时FE1.1S，这个是2.0USB，有点大，但是便宜，貌似几毛钱，JLNK是使用外网开源的，大家需要在使用前下载JLINK的HEX，在主板已经标记，是图中紫色部分。USAET使用是CP2102芯片，这个芯片也挺大的，后期的板子都换成小的了；   
3.USART应该是连接STM32 串口1；  
4.USB供电增加了二极管、保险丝和开关，开关和CAN120电阻放在一起，即图中拨码盘。  

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/JLINK.png "")  
<center>图 3.2 JLINK原理图 </center>  

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/HUB.png "")  
<center>图 3.3 HUB原理图 </center>  

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/JLINK示意图.png "")  
<center>图 3.4 JLINK示意图 </center>  


### 3.1.3 防浪涌雷电CAN口设计

&emsp;这个设计是参考了多个网站学来的，当然雷电试验没有试验，其他的试验了。大家可以参考下。关于这个电路图，我会在陆续出几篇文章说明。电源信号均隔离，并且隔离增加了保险丝；

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/防浪涌雷电设计CAN口.png "")  
![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/CAN1.png "")  
<center>图 3.5 防浪涌雷电设计CAN口 </center>  

### 3.1.4 防浪涌雷电485口设计

&emsp;485是半双工，需要一个开关，这里使用了一个三极管巧妙了避开了这个开关，做个隐形收发自如。  

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/防浪涌雷电设计485口.png "")  
<center>图 3.6 防浪涌雷电设计485口 </center>

### 3.1.5 网口

&emsp; 网口部分我是参考正点原子使用的芯片是LAN8720A，这部分我没有测试，**可能不太好使**。大家注意。


![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/网口.png "")  
<center>图 3.7 网口 </center>

### 3.1.6 继电器

&emsp; 在车体控制可能需要控制电源相关，因此继承了3给继电器，继电器的芯片是如图，大家可以去淘宝搜索即可，我这里使用的是低压5V控制。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/继电器.png "")  `
<center>图 3.8 继电器 </center>

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/继电器链接图.png "")  `
<center>图 3.9 继电器链接图 </center>

### 3.1.6 SD卡

&emsp; SD主要用于日志记录，在车体运行可能会产生大量的日志记录，当然你也可以网络传输到云和上位机等。SD与STM主要靠SD接口连接，速度还是挺快的。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/SD.png "")  `
<center>图 3.10 SD </center>

### 3.1.7 蜂鸣器

&emsp; 没啥好说的，如图所示驱动电路。
![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/蜂鸣器.png "")  `
<center>图 3.11 蜂鸣器 </center>

### 3.1.7 LED

&emsp;LED搞了好多，大家自行使用。一共7个LED，一个5050大小的七彩灯。
1.7个LED大家可以随意使用，已经使用三极管驱动，电流较大；
2.5050七彩灯接入了PWM接口，可以生成多种颜色；
3.貌似有些发热，大家可以选择性的更改阻值大小。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/LED.png "")  
<center>图 3.12 LED </center>

### 3.1.7 FLASH

&emsp;主要使用W25Q256，这个主要用于存储一些车体参数相关，与SD卡不同，SD是容易更换的，存储车体重要参数是不太合适的，但是存在这里是合适的。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/FLASH.png "")  
<center>图 3.13 FLASH </center>

### 3.1.8 STM32周围

1.有复位按键
2.有电子接入MOLEX插座，内置也能焊接充电电子（但是这个貌似撑不了多久）
3.晶振25MHZ，时钟是32.768KHZ
4.外接一个按键，调试使用。
5.USB接入HUB。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/STM32周围示意图.png "")  
<center>图 3.14 STM32周围示意图 </center>

### 3.1.9 传感器

&emsp;这里有三个传感器，一个存储器。都是用IIC通信。这几个传感器，说实话我都没用。车体貌似不需要这几个，大家不用可以忽略。不过在我另一个开源的AHRS主板有详细说明这几个传感器使用。
1.BMX055 是九轴传感器
2.ATC512存储器
3.SHT-20温度传感器（好像没写驱动）
4.BMP180气压传感器（好像没写驱动）

![](传感器.png "")  
<center>图 3.15 传感器 </center>


## 3.2 软件

&emsp;软件主要在MDK编辑，还没转移到CLION或者VSCODE。先介绍文件夹。
1.idea git 忽略
2.doxygen，因为我本想使用这个写说明书的，但是写了部分，没写全，懒啊。大家可以继续用这个鞋。  
3.library，主要放了一些别人的库，比如STM32库等等  
4.os 放一些操作系统，当然，我这里没有；  
5.project 放置工程文件
6.users 放置自己写的源码。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/软件文件夹.png "")  
<center>图 3.16 软件文件夹 </center>

在软件中我写了大部分的说明如下，大家可以看下说明即可。
```C
	/**
		* @brief		Read bytes with IIC.
		* @param[in]	device_address : device address
		* @param[in]	register_address : register address in device
		* @param[out]	p : store data location
		* @param[in]	size : read the size of the data
		* @return  		
		*	+1 	Read successfully \n
		*	-1 	No detection of equipment \n
		*	-2 	Write register failed \n
		* @note 
		* @par Sample
		* @code
		*	u8 byAD = 0XA0,byRA = 0X00;
		*	u8 p[10] = {0};
		*	res_ res = 0; 
		*	res = iic_read_bytes(byAD,byRA,p,10);
		* @endcode
		*/
	extern s32 iic_read_bytes(const u8 device_address,const u8 register_address,u8 *p,const u32 size);
	
```
我这次开源的主要是这部分驱动，你懂得驱动，大家都不愿意写，烦死人。我这里都写好了。具体我局不介绍了，具体的一些细节可能会单独出文章。控制算法部分我没有开源，考虑是研究所和公司的代码，因此没有开源。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/BSP.png "")  
<center>图 3.17 BSP </center>


## 3.3 机械

&emsp;绘制的机械图不知道去哪里去了，因此大家自己绘制把。如图四个边角是内切，这样大家可以做上下两个机械的时候可以直接卡主。主板内部有三个螺丝孔，是个等腰三角形，我尽量考虑强迫症了。

![](https://gitlab.com/blogs_prj/embedded_system_blogs/raw/master/bcb_board/res/机械示意图.png "")  
<center>图 3.17 机械示意图 </center>

